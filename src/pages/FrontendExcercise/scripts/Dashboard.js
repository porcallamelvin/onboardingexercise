import { ref } from "vue";
import { useRouter } from "vue-router";

export default {
  setup() {
    const router = useRouter();
    const cards = ref([
      {
        id: 1,
        title: "Card 1",
        description: "Description 1",
      },
      {
        id: 2,
        title: "Card 2",
        description: "Description 2",
      },
      {
        id: 3,
        title: "Card 3",
        description: "Description 3",
      },
      {
        id: 4,
        title: "Card 4",
        description: "Description 4",
      },
    ]);

    const navigateToTable = () => {
      router.push("/table");
    };

    return {
      cards,
      navigateToTable,
    };
  },
};
