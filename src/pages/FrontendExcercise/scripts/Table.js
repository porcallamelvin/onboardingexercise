import axios from "axios";
import { useRouter } from "vue-router";

export default {
  name: "MyTable",
  setup() {
    const router = useRouter();
    const navigateToForm = () => {
      router.push("/form");
    };

    const toggleActionsMenu = (row) => {
      // Toggle the showActions property for the clicked row
      row.showActions = !row.showActions;
    };

    const editEmployee = (row) => {
      // Handle edit action here
      router.push({ path: "/form", query: { id: row.id } });
    };

    const deleteEmployee = async (row) => {
      try {
        await axios.delete(`http://localhost:3000/users/${row.id}`);
        location.reload(); // Refresh the page after deletion
      } catch (error) {
        console.error("Error deleting employee:", error);
      }
    };

    return {
      toggleActionsMenu,
      editEmployee,
      deleteEmployee,
      navigateToForm,
    };
  },
  data() {
    return {
      selectedDate: "",
      selectedOption: "",
      isDropdownOpen: false,
      isCalendarOpen: false,
      options: ["Option 1", "Option 2", "Option 3"],
      tableData: [],
      columns: [
        {
          name: "Employee ID",
          align: "left",
          label: "EmployeeID",
          field: "employee_id",
          sortable: true,
        },
        {
          name: "Name",
          align: "left",
          label: "Name",
          field: "name",
          sortable: true,
        },
        {
          name: "Email",
          align: "left",
          label: "Email",
          field: "email",
          sortable: true,
        },
        {
          name: "Status",
          align: "left",
          label: "Status",
          field: "status",
          sortable: true,
        },
        {
          name: "Address",
          align: "left",
          label: "Address",
          field: "address",
          sortable: true,
        },
        {
          name: "ellipsis",
          align: "left",
          label: "",
          field: "ellipsis",
          sortable: false, // Disable sorting for this column
        },
      ],
    };
  },
  mounted() {
    // Fetch data from the JSON server using Axios
    axios
      .get("http://localhost:3000/users")
      .then((response) => {
        // Set the fetched data to the tableData
        this.tableData = response.data;
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  },
  methods: {
    toggleDropdown() {
      this.isDropdownOpen = !this.isDropdownOpen;
      this.isCalendarOpen = false;
    },
    selectOption(option) {
      this.selectedOption = option;
      this.isDropdownOpen = false;
    },
    toggleCalendar() {
      this.isCalendarOpen = !this.isCalendarOpen;
      this.isDropdownOpen = false;
    },
  },
};
