import { ref, onMounted } from "vue";
import axios from "axios";
import { useRouter, useRoute } from "vue-router";

export default {
  name: "MyForm",
  setup() {
    const router = useRouter();
    const route = useRoute();

    const name = ref("");
    const employeeId = ref("");
    const email = ref("");
    const currentTask = ref("");
    const description = ref("");
    const status = ref("");
    const houseAddress = ref("");

    const id = ref(null);

    const fetchData = async () => {
      try {
        const response = await axios.get(
          `http://localhost:3000/users/${id.value}`
        );
        const user = response.data;
        name.value = user.name;
        employeeId.value = user.employee_id;
        email.value = user.email;
        status.value = user.status;
        houseAddress.value = user.address;
        // Populate other form fields similarly
      } catch (error) {
        console.error("Error fetching user data:", error);
      }
    };

    onMounted(() => {
      id.value = route.query.id;
      if (id.value) {
        fetchData();
      }
    });

    const navigateBackToTable = () => {
      router.back();
    };

    const submitForm = async () => {
      const user = {
        employee_id: employeeId.value,
        name: name.value,
        email: email.value,
        status: status.value,
        address: houseAddress.value,
      };

      try {
        if (id.value) {
          await axios.put(`http://localhost:3000/users/${id.value}`, user);
        } else {
          user.id = generateRandomId();
          await axios.post("http://localhost:3000/users", user);
        }
        navigateToTable();
      } catch (error) {
        console.error("Error saving user:", error);
      }
    };

    const generateRandomId = () => {
      return Math.random().toString(36).substr(2, 9);
    };

    const navigateToTable = () => {
      router.push("/table");
    };

    return {
      name,
      employeeId,
      email,
      currentTask,
      description,
      status,
      houseAddress,
      submitForm,
      navigateBackToTable,
    };
  },
};
